package com.ankitsingh.weatherforecastapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherScreen<locationListener, location_provider> extends AppCompatActivity {


    TextView tv_temp, tv_dayMax, tv_dayMin, tv_humid, tv_pressure,tv_windspeed;

    final String API_KEY = "315edf15b6af03aac399bdbd3da37759";

    String location_provider = LocationManager.GPS_PROVIDER;
    LocationManager locationManager;
    LocationListener locationListener;
    int requestCode = 101;

    String lon, lat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_screen);

        tv_temp = findViewById(R.id.tv_temp);
        tv_dayMax = findViewById(R.id.tv_dayHigh);
        tv_dayMin = findViewById(R.id.tv_dayLow);
        tv_humid= findViewById(R.id.tv_humid);
        tv_pressure= findViewById(R.id.tv_pressure);
        tv_windspeed= findViewById(R.id.tv_windspeed);


    }

    private void getCurrnetData() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {

                lon = String.valueOf(location.getLongitude());
                lat = String.valueOf(location.getLatitude());

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                WeatherService service = retrofit.create(WeatherService.class);
                Call<Response> call = service.getCurrentWeatherData(lat, lon, API_KEY);
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.code() == 200) {
                            Response Response = response.body();
                            assert Response != null;

                            Float temp = Response.main.getTemp();
                            Integer temperature = (int) (temp - 273.15);
                            tv_temp.setText(String.valueOf(temperature) + "°C");

                            Float temp_max = Response.main.getTempMax();
                            Integer temperature_max = (int) (temp_max - 273.15);
                            tv_dayMax.setText(String.valueOf(temperature_max) + "°C");

                            Float temp_min = Response.main.getTempMin();
                            Integer temperature_min = (int) (temp_min - 273.15);
                            tv_dayMin.setText(String.valueOf(temperature_min) + "°C");
                            tv_humid.setText(String.valueOf(Response.main.getHumidity())+"%");
                            tv_pressure.setText(String.valueOf(Response.main.getPressure())+"mPa");

                            Float speed= Response.wind.getSpeed();
                            tv_windspeed.setText(String.valueOf(speed)+"km/h");
                        } else {
                            Toast.makeText(getApplicationContext(), "Unable to fetch data", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {

                    }
                });
            }

        };


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
            return;
        }
        locationManager.requestLocationUpdates(location_provider, 0, 0, locationListener);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCurrnetData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrnetData();
            } else {
                Toast.makeText(this, "Please Grant Location Permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}