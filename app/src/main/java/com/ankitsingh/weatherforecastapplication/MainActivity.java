package com.ankitsingh.weatherforecastapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText edt_usrname, edt_pwd;
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_usrname = findViewById(R.id.edt_usrname);
        edt_pwd = findViewById(R.id.edt_pswd);
        btn_login = findViewById(R.id.btn_submit);

    }

    public void login(View view) {
        if(edt_usrname.getText().toString().equals("ankit")  && edt_pwd.getText().toString().equals("12345") ){
            Intent intent= new Intent(this, WeatherScreen.class);
            startActivity(intent);
        }

        else{
            Toast.makeText(this, "Enter Correct Credientials", Toast.LENGTH_SHORT).show();
        }
    }
}